# Udata_to_Wikibase

_Fetch a Udata repository and upload its content to a MediaWiki/Wikibase server._

See [Wikidata mapping for datasets](https://www.wikidata.org/wiki/Wikidata:WikiProject_Datasets/Data_Structure/DCAT_-_Wikidata_-_Schema.org_mapping)

## Installation & Configuration

```bash
git clone https://framagit.org/collaborative-open-data/udata-to-wikibase.git
cd udata-to-wikibase/
npm install
```

## Fetch Udata repository

Create a `.env` file and complete it, based on this snippet:

```
UDATA_URL="https://data.gouv2.fr/"
UDATA_DIR="../data/data.gouv.fr/"
```

```bash
npx babel-node --max-old-space-size=2048 src/scripts/udata_to_files.js datasets
npx babel-node src/scripts/udata_to_files.js discussions
npx babel-node src/scripts/udata_to_files.js issues
npx babel-node src/scripts/udata_to_files.js organizations
npx babel-node src/scripts/udata_to_files.js posts
npx babel-node src/scripts/udata_to_files.js reuses
npx babel-node src/scripts/udata_to_files.js users
npx babel-node src/scripts/udata_to_files.js topics
```

## Upload Udata JSON files to Wikibase server

To run this script you must have created a MediaWiki bot on [this page](https://data.gouv2.fr/index.php/Special:BotPasswords/). To do this you must have special rights.

Once you have created your bot on [data.gouv2.fr](https://data.gouv2.fr/), complete the `.env` file with the Wikibase URL, the username and password, based on this snippet:

```
WIKIBASE_URL="https://data.gouv2.fr/"
MEDIAWIKI_BOT_USERNAME=""
MEDIAWIKI_BOT_PASSWORD=""
```

```bash
npx babel-node src/scripts/udata_files_to_wikibase.js
```
