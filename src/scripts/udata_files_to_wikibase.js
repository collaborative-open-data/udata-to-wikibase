import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs"
import rpn from "request-promise-native"
import path from "path"
import url from "url"

import pkg from "../../package.json"
import config from "../config"
import { sleep } from "../helpers"
import { assertValid } from "../validators/assert"
import { validateUrl } from "../validators/core"
import { validateItemCore, validatePropertyCore } from "../validators/entities"

let csrfToken = null
let datasetDistributionPropertyId = null
let downloadItemId = null
// let familyNamePropertyId = null
// let givenNamePropertyId = null
let ofPropertyId = null
let publisherPropertyId = null
let request = null
let technicalDocumentationItemId = null
let urlPropertyId = null

async function login() {
  console.log("Login...")

  let result = await request.post(
    url.resolve(config.wikibase.url, "api.php"),
    {
      form: {
        action: "query",
        format: "json",
        meta: "tokens",
        type: "login",
      },
      json: true,
    },
  )
  assert(result.error === undefined, JSON.stringify(result, null, 2))
  // console.log(JSON.stringify(result, null, 2))
  const loginToken = result.query.tokens.logintoken

  result = await request.post(
    url.resolve(config.wikibase.url, "api.php"),
    {
      form: {
        action: "login",
        format: "json",
        lgname: config.wikibase.user,
        lgpassword: config.wikibase.password,
        lgtoken: loginToken,
      },
      json: true,
    },
  )
  assert(result.error === undefined, JSON.stringify(result, null, 2))
  // console.log(JSON.stringify(result, null, 2))
}

// async function logout() {
//   console.log("Logout...")
//   let result = await request.post(
//     url.resolve(config.wikibase.url, "api.php"),
//     {
//       form: {
//         action: "logout",
//         format: "json",
//       },
//       json: true,
//     },
//   )
//   assert(result.error === undefined, JSON.stringify(result, null, 2))
//   // console.log(JSON.stringify(result, null, 2))
// }

async function main() {
  const optionDefinitions = [
    { name: "dataset", type: String, help: "Slug of dataset to start resume from" },
  ]
  const options = commandLineArgs(optionDefinitions)

  request = rpn.defaults({
    headers: {
      "User-Agent": `${pkg.name}/${pkg.version} (${pkg.repository.url}; ${pkg.author})`,
    },
    jar: true,
  })

  await login()
  await requestCsrfToken()

  datasetDistributionPropertyId = (await upsertPropertyCore({
    datatype: "wikibase-item",
    descriptions: [
      {
        language: "en",
        value: "particular manner of distribution of a data set (database or file) that is publicly available",
      },
      // {
      //   language: "fr",
      //   value: "",
      // },
    ],
    labels: [
      {
        language: "en",
        value: "dataset distribution",
      },
      {
        language: "fr",
        value: "ensemble de données distribuées",
      },
    ],
  })).id
  console.log("dataset distribution property:", datasetDistributionPropertyId)

  downloadItemId = (await upsertItemCore({
    descriptions: [
      {
        language: "en",
        value: "to receive data to a local system from a remote system",
      },
      {
        language: "fr",
        value: "chargement de fichiers informatiques depuis un serveur ou un ordinateur distant",
      },
    ],
    labels: [
      {
        language: "en",
        value: "download",
      },
      {
        language: "fr",
        value: "téléchargement",
      },
    ],
    sitelinks: [{
      site: "enwiki",
      title: "Download",
    }],
  })).id
  console.log("download item:", downloadItemId)

  // familyNamePropertyId = (await upsertPropertyCore({
  //   datatype: "wikibase-item",
  //   descriptions: [
  //     {
  //       language: "en",
  //       value: "surname or last name of a person",
  //     },
  //     {
  //       language: "fr",
  //       value: "nom de famille d'une personne",
  //     },
  //   ],
  //   labels: [
  //     {
  //       language: "en",
  //       value: "family name",
  //     },
  //     {
  //       language: "fr",
  //       value: "nom de famille",
  //     },
  //   ],
  // })).id
  // console.log("family name property:", familyNamePropertyId)

  // givenNamePropertyId = (await upsertPropertyCore({
  //   datatype: "wikibase-item",
  //   descriptions: [
  //     {
  //       language: "en",
  //       value: "first name or another given name of this person; values used with the property shouldn't link disambiguations nor family names",
  //     },
  //     {
  //       language: "fr",
  //       value: "nom individuel d'une personne",
  //     },
  //   ],
  //   labels: [
  //     {
  //       language: "en",
  //       value: "given name",
  //     },
  //     {
  //       language: "fr",
  //       value: "prénom",
  //     },
  //   ],
  // })).id
  // console.log("family name property:", givenNamePropertyId)

  ofPropertyId = (await upsertPropertyCore({
    datatype: "wikibase-item",
    descriptions: [
      {
        language: "en",
        value: "qualifier stating that a statement applies within the scope of a particular item",
      },
      {
        language: "fr",
        value: "qualificateur indiquant que la déclaration s'applique dans l'étendue d'un élément particulier",
      },
    ],
    labels: [
      {
        language: "en",
        value: "of",
      },
      {
        language: "fr",
        value: "de",
      },
    ],
  })).id
  console.log("of property:", ofPropertyId)

  publisherPropertyId = (await upsertPropertyCore({
    datatype: "wikibase-item",
    descriptions: [
      {
        language: "en",
        value: "organization or person responsible for publishing books, periodicals, games or software",
      },
      {
        language: "fr",
        value: "organisation qui est responsable pour la publication du sujet",
      },
    ],
    labels: [
      {
        language: "en",
        value: "publisher",
      },
      {
        language: "fr",
        value: "éditeur",
      },
    ],
  })).id
  console.log("publisher property:", publisherPropertyId)

  technicalDocumentationItemId = (await upsertItemCore({
    descriptions: [
      {
        language: "en",
        value: "description of a technical product and its handling, functionality and architecture",
      },
      {
        language: "fr",
        value: "comprend tous les documents qui décrivent un produit technique",
      },
    ],
    labels: [
      {
        language: "en",
        value: "technical documentation",
      },
      {
        language: "fr",
        value: "documentation technique",
      },
    ],
    sitelinks: [{
      site: "enwiki",
      title: "Technical documentation",
    }],
  })).id
  console.log("technical documentation item:", technicalDocumentationItemId)

  urlPropertyId = (await upsertPropertyCore({
    datatype: "url",
    descriptions: [
      {
        language: "en",
        value: "location of a resource",
      },
      {
        language: "fr",
        value: "endroit d'une ressource",
      },
    ],
    labels: [
      {
        language: "en",
        value: "URL",
      },
      {
        language: "fr",
        value: "URL",
      },
    ],
  })).id
  console.log("URL property:", urlPropertyId)


  const datasets = JSON.parse(fs.readFileSync(path.join(config.udata.dir, "datasets.json"), { encoding: "utf-8" }))
  let skip = !!options.dataset
  for (let dataset of datasets) {
    if (skip) {
      if (dataset.slug === options.dataset) {
        skip = false
      } else {
        continue
      }
    }
    await upsertDataset(dataset)
  }
}

function numericIdFromItemId(itemId) {
  return parseInt(itemId.substring(1))
}

async function postToApi(body) {
  let result = null
  for (let delay of [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 0]) {
    const form = {
      ...body,
      token: csrfToken,
    }
    result = await request.post(
      url.resolve(config.wikibase.url, "api.php"),
      {
        form,
        json: true,
      },
    )
    if (result.error === undefined) {
      return result
    }
    if (
      result.error.messages
      && result.error.messages.some(message =>
        message.name === "wikibase-validator-label-with-description-conflict"
      )
    ) {
      // Duplicate label: Let the caller handle this problem.
      return result
    }
    if (result.error.code === "badtoken") {
      // await logout()
      await login()
      await requestCsrfToken()
      continue
    }
    if (
      result.error.code === "failed-save"
      && result.error.messages
      && result.error.messages.some(message => message.name === "actionthrottledtext")
    ) {
      console.log("A failed-save error occurred:", JSON.stringify(result, null, 2))
      console.log(`Sleeping ${delay} seconds...`)
      sleep(delay)
      continue
    }
    if (result.error.code === "no-external-page") {
      console.log("A no-external-page error occurred:", JSON.stringify(result, null, 2))
      console.log(`Sleeping ${delay} seconds...`)
      sleep(delay)
      continue
    }
    // Unhandled error. Throw an exception.
    break
  }
  assert(result.error === undefined, JSON.stringify(result, null, 2))
}

async function requestCsrfToken() {
  const result = await request.post(
    url.resolve(config.wikibase.url, "api.php"),
    {
      form: {
        action: "query",
        format: "json",
        meta: "tokens",
      },
      json: true,
    },
  )
  assert(result.error === undefined, JSON.stringify(result, null, 2))
  csrfToken = result.query.tokens.csrftoken
  assert(csrfToken !== undefined, JSON.stringify(result, null, 2))
  return csrfToken
}

async function upsertDataset(dataset) {
  const item = await upsertItemCore({
    aliases: dataset.acronym
      ? [{ language: "fr", value: dataset.acronym }]
      : null,
    descriptions: dataset.description
      ? [{ language: "fr", value: dataset.description }]
      : null,
    labels: [{
      language: "fr",
      value: dataset.title,
    }],
    sitelinks: [{
      site: "data.gouv.fr",
      title: `datasets/${dataset.slug}`,
    }]
  })
  const claims = item.claims
  assert(claims !== undefined, JSON.stringify(item, null, 2))
  let lastrevid = item.lastrevid
  assert(lastrevid !== undefined, JSON.stringify(item, null, 2))

  console.log(dataset.slug, item.labels.fr.value, item.id)

  if (dataset.organization || dataset.owner) {
    const publisherItemId = dataset.organization
      ? await upsertOrganization(dataset.organization)
      : await upsertHuman(dataset.owner)
    const existingPublisherClaim = (claims[publisherPropertyId] || [])
      .filter(claim =>
        claim.mainsnak.snaktype === "value"
        && claim.mainsnak.datavalue.value["entity-type"] === "item"
        && claim.mainsnak.datavalue.value.id === publisherItemId
      )[0]
    if (existingPublisherClaim === undefined) {
      // Create new publisher claim.
      const result = await postToApi({
        action: "wbcreateclaim",
        baserevid: lastrevid,
        bot: true,
        entity: item.id,
        format: "json",
        property: publisherPropertyId,
        snaktype: "value",
        // summary: "Adding claim",
        value: JSON.stringify({
          "entity-type": "item",
          "numeric-id": numericIdFromItemId(publisherItemId),
        }),
      })
      lastrevid = result.pageinfo.lastrevid
      assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
      const claim = result.claim
      assert(claim !== undefined, JSON.stringify(result, null, 2))
    }
  }

  {
    const existingDatasetDistributionClaims = (claims[datasetDistributionPropertyId] || [])
      .filter(claim =>
        claim.mainsnak.snaktype === "value"
        && claim.mainsnak.datavalue.value["entity-type"] === "item"
      )

    // Note: The resources are filtered by their ID, because there is a bug in Udata:
    // Sometimes the exact same resource is present more that once.
    const resourcesIds = new Set()
    for (let resource of dataset.resources || []) {
      if (resourcesIds.has(resource.id)) {
        continue
      }
      resourcesIds.add(resource.id)
      const datasetDistributionId = await upsertDatasetDistribution(resource)
      const datasetDistributionClaimIndex = existingDatasetDistributionClaims.findIndex(claim =>
        claim.mainsnak.datavalue.value.id === datasetDistributionId
      )
      if (datasetDistributionClaimIndex >= 0) {
        existingDatasetDistributionClaims.splice(datasetDistributionClaimIndex, 1)
      } else {
        // Create new dataset distribution claim.
        const result = await postToApi({
          action: "wbcreateclaim",
          baserevid: lastrevid,
          bot: true,
          entity: item.id,
          format: "json",
          property: datasetDistributionPropertyId,
          snaktype: "value",
          // summary: "Adding claim",
          value: JSON.stringify({
            "entity-type": "item",
            "numeric-id": numericIdFromItemId(datasetDistributionId),
          }),
        })
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
        const claim = result.claim
        assert(claim !== undefined, JSON.stringify(result, null, 2))
      }
    }

    if (existingDatasetDistributionClaims.length > 0) {
      result = await postToApi({
        action: "wbremoveclaims",
        baserevid: lastrevid,
        bot: true,
        claim: existingDatasetDistributionClaims.map(claim => claim.id).join("|"),
        format: "json",
        // summary: ...
      })
      lastrevid = result.pageinfo.lastrevid
      assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
    }
  }

  return item.id
}

async function upsertDatasetDistribution(resource) {
  const item = await upsertItemCore({
    descriptions: resource.description
      ? [{ language: "fr", value: resource.description }]
      : null,
    labels: [{
      language: "fr",
      value: resource.title || "Resource sans nom",
    }],
    sitelinks: [{
      site: "data.gouv.fr",
      title: `resources/${resource.id}`,
    }]
  })
  const claims = item.claims
  assert(claims !== undefined, JSON.stringify(item, null, 2))
  let lastrevid = item.lastrevid
  assert(lastrevid !== undefined, JSON.stringify(item, null, 2))

  console.log(resource.id, item.labels.fr.value, item.id)

  {
    const [resourceUrl, error] = validateUrl(resource.url)
    if (error !== null) {
      console.log(`Skipping URL "${resourceUrl}", because: ${error}`)
    } else {
      let urlClaim = (claims[urlPropertyId] || [])
        .filter(claim =>
          claim.mainsnak.snaktype === "value"
          && claim.mainsnak.datavalue.value === resourceUrl
        )[0]
      if (urlClaim === undefined) {
        // Create new publisher claim.
        const result = await postToApi({
          action: "wbcreateclaim",
          baserevid: lastrevid,
          bot: true,
          entity: item.id,
          format: "json",
          property: urlPropertyId,
          snaktype: "value",
          // summary: "Adding claim",
          value: JSON.stringify(resourceUrl),
        })
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
        urlClaim = result.claim
        assert(urlClaim !== undefined, JSON.stringify(result, null, 2))
      }

      switch (resource.type) {
        case "api":
          break
        case "code":
          break
        case "documentation":
          // Create "of technical documentation" qualifier if it doesn't exist yet.
          if (!((urlClaim.qualifiers || {})[ofPropertyId] || []).some(qualifier =>
            qualifier.snaktype === "value"
            && qualifier.datavalue.value["entity-type"] === "item"
            && qualifier.datavalue.value.id === technicalDocumentationItemId
          )) {
            const result = await postToApi({
              action: "wbsetqualifier",
              baserevid: lastrevid,
              bot: true,
              claim: urlClaim.id,
              format: "json",
              property: ofPropertyId,
              snaktype: "value",
              // summary: "Adding qualifier",
              value: JSON.stringify({
                "entity-type": "item",
                "numeric-id": numericIdFromItemId(technicalDocumentationItemId),
              }),
            })
            assert(result.error === undefined, JSON.stringify(result, null, 2))
            // console.log(JSON.  stringify(result, null, 2))
            lastrevid = result.pageinfo.lastrevid
            assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
          }
          break
        case "main":
          // Create "of download" qualifier if it doesn't exist yet.
          if (!((urlClaim.qualifiers || {})[ofPropertyId] || []).some(qualifier =>
            qualifier.snaktype === "value"
            && qualifier.datavalue.value["entity-type"] === "item"
            && qualifier.datavalue.value.id === downloadItemId
          )) {
            const result = await postToApi({
              action: "wbsetqualifier",
              baserevid: lastrevid,
              bot: true,
              claim: urlClaim.id,
              format: "json",
              property: ofPropertyId,
              snaktype: "value",
              // summary: "Adding qualifier",
              value: JSON.stringify({
                "entity-type": "item",
                "numeric-id": numericIdFromItemId(downloadItemId),
              }),
            })
            assert(result.error === undefined, JSON.stringify(result, null, 2))
            // console.log(JSON.  stringify(result, null, 2))
            lastrevid = result.pageinfo.lastrevid
            assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
          }
          break
        case "other":
          break
        case "update":
          break
        default:
          throw `Unknown resource type: ${resource.type}`
      }
    }
  }

  return item.id
}

async function upsertHuman(user) {
  const item = await upsertItemCore({
    labels: [{
      language: "fr",
      value: `${user.first_name} ${user.last_name}`,
    }],
    sitelinks: [{
      site: "data.gouv.fr",
      title: `users/${user.slug}`,
    }]
  })
  const claims = item.claims
  assert(claims !== undefined, JSON.stringify(item, null, 2))
  let lastrevid = item.lastrevid
  assert(lastrevid !== undefined, JSON.stringify(item, null, 2))

  console.log(user.slug, item.labels.fr.value, item.id)

  return item.id
}

// Update or create an item core (without claims).
async function upsertItemCore(data) {
  data = assertValid(validateItemCore(data))
  return upsertValidEntityCore("item", data)
}

async function upsertOrganization(organization) {
  const item = await upsertItemCore({
    aliases: organization.acronym
      ? [{ language: "fr", value: organization.acronym }]
      : null,
    // descriptions: organization.description
    //   ? [{ language: "fr", value: organization.description }]
    //   : null,
    labels: [{
      language: "fr",
      value: organization.name,
    }],
    sitelinks: [{
      site: "data.gouv.fr",
      title: `organizations/${organization.slug}`,
    }]
  })
  const claims = item.claims
  assert(claims !== undefined, JSON.stringify(item, null, 2))
  let lastrevid = item.lastrevid
  assert(lastrevid !== undefined, JSON.stringify(item, null, 2))

  console.log(organization.slug, item.labels.fr.value, item.id)

  return item.id
}

async function upsertPropertyCore(data) {
  data = assertValid(validatePropertyCore(data))
  return upsertValidEntityCore("property", data)
}

async function upsertValidEntityCore(entityType, data) {
  const firstLabel = { ...data.labels[0] }

  let entity = null
  let result = null
  if (data.sitelinks === undefined) {
    result = await postToApi({
      action: "wbsearchentities",
      format: "json",
      language: firstLabel.language,
      limit: 1,
      search: firstLabel.value,
      type: entityType,
    })
    if (result.search.length > 0 && data.labels.map(label => label.value).includes(result.search[0].label)) {
      result = await postToApi({
        action: "wbgetentities",
        format: "json",
        ids: result.search[0].id,
      })
      assert(result.entities)
      assert(Object.keys(result.entities).length === 1)
      entity = Object.values(result.entities)[0]
    }
  } else {
    const firstSiteLink = data.sitelinks[0]
    result = await postToApi({
      action: "wbgetentities",
      format: "json",
      sites: firstSiteLink.site,
      titles: firstSiteLink.title,
    })
    assert(result.entities)
    assert(Object.keys(result.entities).length === 1)
    entity = Object.values(result.entities)[0]
  }
  if (entity === null || entity.missing !== undefined) {
    // Wikibase entity is missing. Create it.
    for (let i = 0; ; i++) {
      result = await postToApi({
        action: "wbeditentity",
        data: JSON.stringify(data, null, 2),
        format: "json",
        new: entityType,
      })
      if (
        result.error
        && result.error.messages
        && result.error.messages.some(message => message.name === "wikibase-validator-label-with-description-conflict")
      ) {
        // An entity with the same name already exists.
        data.labels[0].value = `${firstLabel.value} (${i + 2})`
        continue
      }
      break
    }
    entity = result.entity
  } else {
    // Check if existing entity needs to be updated.
    const dataChanges = {}
    if (data.aliases !== undefined) {
      if (data.aliases.some(dataAlias => {
        const entityAliases = entity.aliases[dataAlias.language]
        if (entityAliases === undefined) {
          return true
        }
        return !entityAliases.some(entityAlias => entityAlias.value === dataAlias.value)
      })) {
        // Some aliases in data are not present in entity. Add them all.
        dataChanges.aliases = data.aliases.map(alias => {
          alias.add = ""
          return alias
        })
      }
    }
    if (data.datatype !== undefined) {
      assert.strictEqual(
        data.datatype,
        entity.datatype,
        `Existing property ${JSON.stringify(entity, null, 2)} has datatype "${entity.datatype}" different from "${data.datatype}"`)
    }
    if (data.descriptions !== undefined) {
      if (data.descriptions.some(description => {
        const entityDescription = entity.descriptions[description.language]
        return entityDescription === undefined || description.value !== entityDescription.value
      })) {
        // Some descriptions in data are missing from entity. Add them.
        dataChanges.descriptions = data.descriptions
      }
    }
    if (data.labels.some(label => {
      const entityLabel = entity.labels[label.language]
      return entityLabel === undefined || label.value !== entityLabel.value
    })) {
      // Some labels in data are missing from entity. Add them.
      dataChanges.labels = data.labels
    }
    if (data.sitelinks !== undefined) {
      if (data.sitelinks.some(siteLink => {
        const entitySiteLink = entity.sitelinks[siteLink.site]
        return entitySiteLink === undefined || siteLink.title !== entitySiteLink.title
      })) {
        // Some sitelinks in data are missing from entity. Add them.
        dataChanges.sitelinks = data.sitelinks
      }
    }
    if (Object.keys(dataChanges).length > 0) {
      // Update entity.
      console.log(`Updating entity ${entity.id}:`, JSON.stringify(dataChanges, null, 2))
      for (let i = 0; ; i++) {
        result = await postToApi({
          action: "wbeditentity",
          data: JSON.stringify(dataChanges, null, 2),
          format: "json",
          id: entity.id,
        })
        if (
          result.error
          && result.error.messages
          && result.error.messages.some(message => message.name === "wikibase-validator-label-with-description-conflict")
        ) {
          if (dataChanges.labels !== undefined) {
            // An entity with the same name already exists.
            dataChanges.labels[0] = {...data.labels[0]}
            dataChanges.labels[0].value = `${firstLabel.value} (${i + 2})`
            continue
          } else if (dataChanges.descriptions !== undefined) {
            // An entity with the same label & description already exists.
            dataChanges.labels = [...data.labels]
            dataChanges.labels[0] = { ...data.labels[0] }
            dataChanges.labels[0].value = `${firstLabel.value} (${i + 2})`
            continue
          }
        }
        break
      }
      entity = result.entity
    }
  }
  return entity
}

main()
  .catch(error => {
    console.log(error.stack || error)
    process.exit(1)
  })
