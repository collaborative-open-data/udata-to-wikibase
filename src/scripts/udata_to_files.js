import assert from "assert"
import commandLineArgs from "command-line-args"
import config from "../config"
import fs from "fs"
import fetch from "node-fetch"
import path from "path"
import url from "url"

const dataTypes = ["datasets", "discussions", "issues", "organizations", "posts", "reuses", "users", "topics"]

async function fetchData(dataType) {
  let data = []
  let dataUrl = url.resolve(config.udata.url, `api/1/${dataType}/?page_size=100`)
  while (dataUrl) {
    let response = null
    console.log(`Fetching "${dataUrl}"`)
    try {
      response = await fetch(dataUrl)
    } catch (error) {
      console.log(`An error occured while fetching "${dataUrl}":`, error)
      break
    }
    const result = await response.json()
    if (result.data) {
      data = [...data, ...result.data]
    }
    dataUrl = result.next_page
  }

  if (!fs.existsSync(config.udata.dir)) {
    fs.mkdirSync(config.udata.dir)
  }
  fs.writeFileSync(path.join(config.udata.dir, `${dataType}.json`), JSON.stringify(data, null, 2))
}

const optionsDefinition = [{ defaultOption: true, name: "type", type: String }]
const options = commandLineArgs(optionsDefinition)
assert(dataTypes.includes(options.type), `Type must belong to ${JSON.stringify(dataTypes, null, 2)}`)

fetchData(options.type).catch(error => {
  console.log(error.stack || error)
  process.exit(1)
})
