require("dotenv").config()

import { validateConfig } from "./validators/config"

const config = {
  udata: {
    dir: process.env.UDATA_DIR || "../data/data.gouv.fr/",
    url: process.env.UDATA_URL || "https://data.gouv.fr/",
  },
  wikibase: {
    url: process.env.WIKIBASE_URL || "https://data.gouv2.fr/",
    user: process.env.MEDIAWIKI_BOT_USERNAME || "MEDIAWIKI_BOT_USERNAME",
    password: process.env.MEDIAWIKI_BOT_PASSWORD || "MEDIAWIKI_BOT_USERNAME",
  },
}

const [validConfig, error] = validateConfig(config)
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(validConfig, null, 2)}\nError:\n${JSON.stringify(error, null, 2)}`
  )
  process.exit(-1)
}

export default validConfig
